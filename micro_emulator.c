// Microprocessor Building Blocks for Computer Emulators
// Description.: Develop the math and basic building blocks to emulate 1-bit, 4-bit, and 8-bit microprocessors.
// By..........: John Mark Mobley
// Start Date..: 03-06-2023
// Last Updated: 03-06-2023
//
// Note this code is untested
//
// Note:
//   This is pseudo code and it is completely untested.
//   At this time is meant more to offer ideas than anything else.

// Idea #1:
// You can compile a simple assembly language program and run it compiled speeds.
// Ex: Make a Branch Not Zero instruction
//   void asm_test()
//   {
//       unsigned char loop_counter;
//
//       loop_counter = 100;
//     loop_00:
//       loop_counter = subtract_with_carry_8_bits(loop_counter, 1, 0);
//       if (test_not_zero()) goto loop_00;
//       return;
//   }

// Newsletter
// https://www.glensideccc.com/wp-content/uploads/2020/03/gccc35_2nl.pdf
// See page 12 and 15 by John Mark Mobley

// Constants

// CPU type
// Should we just do 1-bit, 4-bit and 8-bit CPUs?
#define CPU_TYPE_INTEL_4004        0
#define CPU_TYPE_INTEl_8008        1
#define CPU_TYPE_INTEL_8080        2
#define CPU_TYPE_ZILOG_Z80         3
#define CPU_TYPE_MOTOROLA_6803     4
#define CPU_TYPE_MOTOROLA_6809     5
#define CPU_TYPE_HITACHI_6309      6
#define CPU_TYPE_MOS_6502          7
#define CPU_TYPE_RCA_1802          8
#define CPU_TYPE_TI_TMS9900        9
#define CPU_TYPE_USER_SPECIFIC_00 10
#define CPU_TYPE_DEBUG            11
#define CPU_TYPE_END_OF_LIST      12

#define BCD_ADD_METHOD_BASIC_OVERFLOW   0
#define BCD_ADD_METHOD_MAX_VALUE_OF_19  1
#define BCD_ADD_METHOD_USE_LOOKUP_TABLE 2

#define SUBTRACT_METHOD_USE_1_BIT_BINARY_LOGIC      0
#define SUBTRACT_METHOD_USE_1_BIT_LOOKUP_TABLE      1
#define SUBTRACT_METHOD_USE_BASIC_MATH              2
#define SUBTRACT_METHOD_USE_ADD_IDEA_1              3
#define SUBTRACT_METHOD_USE_ADD_IDEA_2              4
#define SUBTRACT_METHOD_INTRODUCE_A_BUG_FOR_TESTING 5
#define SUBTRACT_METHOD_END_OF_LIST                 6

#define BIG_ENDIAN_CPU    0
#define LITTLE_ENDIAN_CPU 1

// Global variables
  unsigned cahr cpu_type = CPU_TYPE_MOTOROLA_6809;
  unsigned char half_carry_flag = 0;
  unsigned char carry_flag = 0;
  unsigned char sign_flag = 0;
  unsigned char zero_flag = 0;
  unsigned char overflow_flag = 0;
  unsigned char bcd_overflow_flag = 0;
  unsigned char bcd_input_out_of_range_flag = 0;
  unsigned char bcd_max_input_value_of_9 = 0;
  unsigned char bcd_add_methond = BCD_ADD_METHOD_BASIC_OVERFLOW;
  unsigned char subtract_with_normal_carry_logic = 1;
  unsigned char endian_type = BIG_ENDIAN_CPU;
  unsigned char subtract_method = SUBTRACT_METHOD_USE_1_BIT_BINARY_LOGIC;
  unsigned char dummy_value_to_prevent_optimization = 0;

void set_cpu (unsigned char type)
{
  cpu_type = type;
  if (cpu_type >= CPU_TYPE_END_OF_LIST)
  {
    cpu_type = CPU_TYPE_END_OF_LIST - 1;
  }

  if(type == CPU_TYPE_ZILOG_Z80)
  {
    bcd_add_methond = BCD_ADD_METHOD_BASIC_OVERFLOW;
    subtract_wtih_normal_carry_logic = 1; // TRUE
    bcd_max_input_value_of_9 = 0; // FALSE
    endian_type = LITTLE_ENDIAN_CPU;
  }

  if(type == CPU_TYPE_MOTOROLA_6809)
  {
    bcd_add_methond = BCD_ADD_METHOD_BASIC_OVERFLOW;
    subtract_wtih_normal_carry_logic = 1; // TRUE
    bcd_max_input_value_of_9 = 0; // FALSE
    endian_type = BIG_ENDIAN_CPU;
  }

  if(type == CPU_TYPE_DEBUG)
  {
    // Test Microchip subtract logic
    bcd_add_methond = BCD_ADD_METHOD_BASIC_OVERFLOW;
    subtract_wtih_normal_carry_logic = 0; // FALSE
    bcd_max_input_value_of_9 = 0; // FALSE
    endian_type = BIG_ENDIAN_CPU;
  }
}

void set_subtract_method (unsigned char type)
{
  subtract_method = type;
  if (subtract_method >= SUBTRACT_METHOD_END_OF_LIST)
  {
    subtract_method = SUBTRACT_METHOD_USE_1_BIT_BINARY_LOGIC;
  }
}

// test_zero
// Ex: Make a Branch Not Zero instruction
//     loop_counter = 100;
//   loop_00:
//     loop_counter = subtract_with_carry_8_bits(loop_counter, 1, 0);
//     if (test_not_zero()) goto loop_00;
//     return;
unsigned char test_not_zero()
{
  return not_bit(zero_flag);
}

unsigned char get_lower_bit_of_byte(unsigned char a)
{
  // a = a bitwise_and 0x01
  a = a & 0x01;
  return a;
}

// invert the bit
unsigned char not_bit(unsigned char a)
(
  a=get_lower_bit_of_byte(a)
  // a=a bitwise_xor 0x01;
  a = a ^ 0x01;
  return a;
)

unsigned char get_lower_4_bits_of_byte(unsigned char a)
{
  // a = a bitwise_and 0x0F
  a = a & 0x0F;
  return a;
}

unsigned char get_upper_4_bits_of_byte(unsigned char a)
{
  // shift a right 4 bits
  // a = a / 16;
  a = a >> 4;
  return a;
}

unsigned char get_1_bit_carry(unsigned char a)
{
  // make the value of a to be a 2 bit value
  // a = a bitwise_and 0x1F;
  a = a & 0x03;
  // shift a right 1 bits
  // a = a / 2;
  a = a >> 1;
  return a;
}

unsigned char get_4_bit_carry(unsigned char a)
{
  // make the value of a to be a 5 bit value
  // a = a bitwise_and 0x1F;
  a = a & 0x1F;
  // shift a right 4 bits
  // a = a / 16;
  a = a >> 4;
  return a;
}

unsigned char full_subtractor_1_bit(unsigned char a, unsigned char b, unsigned char carry)
{
  unsigned char data;
  unsigned char results;
  a = get_lower_bit_of_byte(a);
  b = get_lower_bit_of_byte(b);
  carry = get_lower_bit_of_byte(carry);

  if (subtract_method == SUBTRACT_METHOD_USE_1_BIT_BINARY_LOGIC)
  {
    // data = a bitwise_xor b bitwise_xor carry
    data = a ^ b ^ carry;

    // & => bitwise_and, | => bitwise_or
    carry = (not_bit(a) & carry) | (not_bit(a) & b) | (b & carry);

    // results = carry * 2 + data;
    results = carry;
    // shift results left by 1 bit
    results = results << 1; // results = results * 2
    results = results | data;
  }
  else // SUBTRACT_METHOD_USE_1_BIT_LOOKUP_TABLE
  {
    unsigned char index;

    // I am not sure if these should be static
    // static unsigned char const data_table[8]  = {0, 1, 1, 0, 1, 0, 0, 1}; // Diff Table
    // static unsigned char const carry_table[8] = {0, 1, 1, 1, 0, 0, 0, 1}; // Barrow Table
    static unsigned char const results_table[8]  = {0, 3, 3, 2, 1, 0, 0, 3}; // Results Table

    // Computer the array index
    // index = a * 4 + b * 2 + c;
    index = a;
    index = index << 1;
    index = index | b;
    index = index << 1;
    index = index | c;

    // data = data_table[index];
    // carry = carry_table[index];
    // results = carry * 2 + data;
    results = results_table[index];
  }

  return results; // Note results is a 2-bit value (carry_out/barrow_out + 1-bit answer/data/diff)
}
 
// sign extend a 4-bit value to an 8-bit value
signed char convert_4_bit_value_to_a_signed_byte (a)
{
  a=get_lower_4_bits_of_byte(a);
  // if (a bitwise_and 0x08)
  if (a & 0x08)
  {
   // a = a bitwise_or 0xF0;
   a = a | 0xF0;
  }
  return a;
}

// sign extend a 5-bit value to an 8-bit value
signed char convert_5_bit_value_to_a_signed_byte (a)
{
  // make a 5-bit balue
  a = a & 0x1F; // use bitwise_and to mask
  // if (a bitwise_and 0x10)
  if (a & 0x10) // if bit-4 is set then set bit-5, bit-6, and bit-7 also
  {
   // a = a bitwise_or 0xE0;
   a = a | 0xE0;
  }
  return a;
}

unsigned char ones_complement_4_bit(unsigned char a)
{
  a = get_lower_4_bits_of_byte(a)
  // invert the lower 4 bits
  // a = a bitwise_xor 0x0F
  a = a ^ 0x0F;
  return a;
}

unsigned char twos_complement_4_bit(unsigned char a)
{
  // this is the easy way
  // a = get_lower_4_bits_of_byte(convert_4_bit_value_to_a_signed_byte(-a));

  // this way is more educational
  a = get_lower_4_bits_of_byte(a);
  a = ones_complement_4_bit(a)
  // do the two's complement
  a = get_lower_4_bits_of_byte(a+1)
  return a;
}

unsigned char neg_4_bit(unsigned char a)
{
  // a = get_lower_4_bits_of_byte(convert_4_bit_value_to_a_signed_byte(-a));
  a = twos_complement_4_bit(a);
  return a;
}

unsigned char add_with_carry_4_bits(unsigned char a, unsigned char b, unsigned char carry_in)
{
  unsigned char results;
  a=get_lower_4_bits_of_byte(a);
  b=get_lower_4_bits_of_byte(b);
  carry_in = get_lower_bit_of_byte(carry_in);
  results = a + b + carry_in;
  return results; // Note results is a 5-bit value (carry_out + 4-bit answer)
}

unsigned char subtract_with_carry_4_bits(unsigned char a, unsigned char b, unsigned char carry)
{
  // compute: results = a - b - carry

  unsigned char results;
 
  a = get_lower_4_bits_of_byte(a);
  b = get_lower_4_bits_of_byte(b);
  carry = get_lower_bit_of_byte(carry);

  if (subtract_wtih_normal_carry_logic == 0)
  {
    carry = not_bit(carry);
  }

  if ((subtract_method == SUBTRACT_METHOD_USE_1_BIT_BINARY_LOGIC) ||
      (subtract_method == SUBTRACT_METHOD_USE_1_BIT_LOOKUP_TABLE))
  {
    // use 4 full 1 bit subtractors to make a 4 bit subtractor
    unsigned char temp;
    unsigned char a0; // bits of a
    unsigned char a1; // "
    unsigned char a2; // "
    unsigned char a3; // "
    unsigned char b0; // bits of b
    unsigned char b1; // "
    unsigned char b2; // "
    unsigned char b3; // "
    unsigned char r0; // bits of results
    unsigned char r1; // "
    unsigned char r2; // "
    unsigned char r3; // "

    // break a into bits ( >> 1 => shift right by 1 bit) 
    temp = a;
    a0 = get_lower_bit_of_byte(temp);
    temp = temp >> 1;
    a1 = get_lower_bit_of_byte(temp);
    temp = temp >> 1;
    a2 = get_lower_bit_of_byte(temp);
    temp = temp >> 1;
    a3 = get_lower_bit_of_byte(temp);

    // break b into bits
    temp = b;
    b0 = get_lower_bit_of_byte(temp);
    temp = temp >> 1; // shift right or divide by 2
    b1 = get_lower_bit_of_byte(temp);
    temp = temp >> 1; // shift right or divide by 2
    b2 = get_lower_bit_of_byte(temp);
    temp = temp >> 1; // shift right or divide by 2
    b3 = get_lower_bit_of_byte(temp);
 
    // computre r0
    results = full_subtractor_1_bit(a0, b0, carry);
    r0 = get_lower_bit_of_byte(results);
    carry = get_1_bit_carry(results);
  
    // compute r1
    results = full_subtractor_1_bit(a1, b1, carry);
    r1 = get_lower_bit_of_byte(results);
    carry = get_1_bit_carry(results);
  
    // compute r2
    results = full_subtractor_1_bit(a2, b2, carry);
    r2 = get_lower_bit_of_byte(results);
    carry = get_1_bit_carry(results);
  
    // compute r3
    results = full_subtractor_1_bit(a3, b3, carry);
    r3 = get_lower_bit_of_byte(results);
    carry = get_1_bit_carry(results);
  
    // give a 5 bit results
    results = carry;
    results = results << 1; // shift left or multiply by 2
    results = results | r3; // bitwise_or
    results = results << 1; // shift left or multiply by 2
    results = results | r2; // bitwise_or
    results = results << 1; // shift left or multiply by 2
    results = results | r1; // bitwise_or
    results = results << 1; // shift left or multiply by 2
    results = results | r0; // bitwise_or
  }
  else if (subtract_method == SUBTRACT_METHOD_USE_BASIC_MATH)
  {
    results = convert_4_bit_value_to_a_signed_byte(a) - convert_4_bit_value_to_a_signed_byte(b) - carry;

    // make the value of results to be a 5 bit value
    // results = results bitwise_and 0x1F;
    results = results & 0x1F;
  }
  else if (subtract_method == SUBTRACT_METHOD_USE_ADD_IDEA_1)
  {
    // see if you can crate a subtract by only using adds
    if (carry)
    {
      // the twos_complement below will add 1 but the ones_complement will not so I think this will crate a borrow
      results = a + ones_complement_4_bit(b);
    }
    else
    {
      // (a - b) == (a + -b)
      results = a + twos_complement_4_bit(b);
    }
  }
  else if (subtract_method == SUBTRACT_METHOD_USE_ADD_IDEA_2)
  {
    // see if you can crate a subtract by only using adds

    results = a + ones_complement_4_bit(b) + not_bit(carry);

    results = results ^ 0x10; // use a bitwise_xor to invert the carry

    // make the value of results to be a 5 bit value
    // results = results bitwise_and 0x1F;
    results = results & 0x1F;
  }
  else if (subtract_method == SUBTRACT_METHOD_INTRODUCE_A_BUG_FOR_TESTING)
  {
    results = convert_4_bit_value_to_a_signed_byte(a) - convert_4_bit_value_to_a_signed_byte(b) - carry;

    // make the value of results to be a 5 bit value
    // results = results bitwise_and 0x1F;
    results = results & 0x1F;

    // Create a bug in the software to test that debug unit test logic is working
    if (results == 0)
    {
      results = 1;
    }
  }
  else
  {
    results = 0;
  }

  if (subtract_wtih_normal_carry_logic == 0)
  {
    results = results ^ 0x10; // use a bitwise_xor to invert the carry
  }

  return results; // Note results is a 5-bit value (carry_out + 4-bit answer)
}

unsigned char bcd_add_with_carry_4_bits(unsigned char a, unsigned char b, unsigned char carry_in)
{
  unsigned char results;
  a=get_lower_4_bits_of_byte(a);
  b=get_lower_4_bits_of_byte(b);
  carry_in = get_lower_bit_of_byte(carry_in);
  results = a + b + carry_in;
  bcd_overflow_flag = 0;
  if (results > 19) bcd_overflow_flag = 1;
  bcd_input_out_of_range_flag = 0;
  if (a > 9) bcd_input_out_of_range_flag = 1;
  if (b > 9) bcd_Input_out_of_range_flag = 1;
  if (results > 9) results = results + 6;

  // because of overflow conditions we need make sure we make results a 5-bit value 
  // make the value of results to be a 5 bit value
  // results = results bitwise_and 0x1F;
  results = results & 0x1F

  return results; // Note results is a 5-bit value (carry_out + 4-bit answer)
}

unsigned char bcd_nines_complement_4_bit(unsigned char a)
{
  a=get_lower_4_bits_of_byte(a);
  if(a > 9)
  {
    if(bcd_max_input_value_of_9)
    {
      a = 9;
    }
    bcd_input_out_of_range_flag = 1;
  }
  a = subtract_with_carry_4_bits(9, a, 0)
  a = get_lower_4_bits_of_byte(a);
  return a;
}

unsigned cahr bcd_tens_complement_4_bit(unsigned char a)
{
  a=get_lower_4_bits_of_byte(a);
  if(a > 9)
  {
    if(bcd_max_input_value_of_9)
    {
      a = 9;
    }
    bcd_input_out_of_range_flag = 1;
  }
  a = bcd_nines_complement_4_bit(a);
  a = get_lower_4_bits_of_byte(a+1);
  return a;
}

unsigned char bcd_subtract_with_carry_4_bits(unsigned char a, unsigned char b, unsigned char carry_in)
{
  unsigned char results;
  a=get_lower_4_bits_of_byte(a);
  if(a > 9)
  {
    if(bcd_max_input_value_of_9)
    {
      a = 9;
    }
    bcd_input_out_of_range_flag = 1;
  }

  b=get_lower_4_bits_of_byte(b);
  if(b > 9)
  {
    if(bcd_max_input_value_of_9)
    {
      b = 9;
    }
    bcd_input_out_of_range_flag = 1;
  }
  carry_in = get_lower_bit_of_byte(carry_in);

  if (carry_in)
  {
    results = a + bcd_nines_complement_4_bit(b);
  }
  else
  {
    results = a + bcd_tens_complement_4_bit(b);
  }

  bcd_overflow_flag = 0;
  if (results > 19) bcd_overflow_flag = 1;
  if (results > 9) results = results + 6;

  // because of overflow conditions we need make sure we make results a 5-bit value 
  // make the value of results to be a 5 bit value
  // results = results bitwise_and 0x1F;
  results = results & 0x1F

  return results; // Note results is a 5-bit value (carry_out + 4-bit answer)
}

unsigned char make_byte_with_upper_4_bits_and_lower_4_bits(unsigned char upper_4_bits, lower_4_bit)
{
  unsigned char results;
  upper_4_bits = get_lower_4_bits_of_byte(upper_4_bits);
  lower_4_bits = get_lower_4_bits_of_byte(lower_4_bits);
  // shift upper 4 bits left 4 times
  // answer = upper_4_bits * 16;
  answer = upper_4_bits << 4;
  answer = answer + ower_4_bits;
  return answer;
}

unsigned char get_8_bit_sign_bit(unsigned char a)
{
  // shift a right 7 bits
  // a = a / 128
  a = a >> 7;
  return a;
}

unsigned char add_with_carry_8_bits(unsigned char a, unsigned char b, unsigned char carry_in)
{
  unsigned char AL;
  unsigned char AH;
  unsigned char BL;
  unsigned char BH;
  unsigned char results;
  unsigned char sign_of_a;
  unsigned char sigh_of_b;
  sign_of_a=get_8_bit_sign_bit(a);
  sign_of_b=get_8_bit_sign_bit(b);
  AL=get_lower_4_bits_of_byte(a);
  AH=get_upper_4_bits_of_byte(a);
  BL=get_lower_4_bits_of_byte(b);
  BH=get_upper_4_bits_of_byte(b);
  results = add_with_carry_4_bits(AL, BL, carry_in);
  half_carry_flag=get_4_bit_carry(result);
  RL = get_lower_4_bits_or_byte(results);
  results = add_with_carry_4_bits(AH, BH, half_carry_flag);
  carry_flag=get_4_bit_carry(results);
  RH=get_lower_4_bits_of_byte(results);
  results=make_byte_with_upper_4_bits_and_lower_4_bits(RH, RL);
  sign_flag = get_8_bit_sign_bit(results);
  zero_flag = get_lower_bit_of_byte(results == 0);
  // & => bitwise_and, | => bitwise_or
  overflow_flag = (sign_of_a & sign_of_b & not_bit(sign_flag)) | (not_bit(sign_of_a) & not_bit(sign_of_b) & sign_flag);
 
  return results;
}

unsigned char subtract_with_carry_8_bits(unsigned char a, unsigned char b, unsigned char carry_in)
{
  unsigned char AL;
  unsigned char AH;
  unsigned char BL;
  unsigned char BH;
  unsigned char results;
  unsigned char sign_of_a;
  unsigned char sigh_of_b;
  sign_of_a=get_8_bit_sign_bit(a);
  sign_of_b=get_8_bit_sign_bit(b);
  AL=get_lower_4_bits_of_byte(a);
  AH=get_upper_4_bits_of_byte(a);
  BL=get_lower_4_bits_of_byte(b);
  BH=get_upper_4_bits_of_byte(b);
  results = subtract_with_carry_4_bits(AL, BL, carry_in);
  half_carry_flag=get_4_bit_carry(result);
  RL = get_lower_4_bits_or_byte(results);
  results = subtract_with_carry_4_bits(AH, BH, half_carry_flag);
  carry_flag=get_4_bit_carry(results);
  RH=get_lower_4_bits_of_byte(results);
  results=make_byte_with_upper_4_bits_and_lower_4_bits(RH, RL);
  sign_flag = get_8_bit_sign_bit(results);
  zero_flag = get_lower_bit_of_byte(results == 0);
  // & => bitwise_and, | => bitwise_or
  overflow_flag = (sign_of_a & not_bit(sign_of_b) & not_bit(sign_flag)) | (not_bit(sign_of_a) & sign_of_b & sign_flag);
  return results;
}

unsigned long long int subtract_4_bit_time_test (void)
{
  unsigned char carry_in;
  unsigned char a;
  unsigned char b;
  unsigned char unsigned_test_results;
  unsigned long long int strt_time;
  unsigned long long int end_time;
  
  while(1)
  {
    start_time = system_timer();
    for(carry_in = 0; carry_in = carry_in + 1; carry_in <= 1)
    {
      for(a = 0; a = a + 1; a <= 0x0F)
      {
        for(b=0; b = b + 1; b <= 0x0F)
        {
          unsigned_test_results = subtract_with_carry_4_bits(a, b, carry_in);
          dummy_value_to_prevent_optimization = unsigned_test_results;
        }
      }
    }
    end_time = system_timer();

    // test of overflow error
    if (start_time < endtime_time) break;
  }

  return end_time - start_time;
}

int subtract_4_bit_unit_test (void)
{
  unsigned char carry_in;
  unsigned char a;
  signed char   signed_a;
  unsigned char b;
  signed char   signed_b;
  unsigned char unsigned_test_results;
  signed char   signed_test_results;
  unsigned char unsigned_expected_results;
  signed char   signed_expected_results;
  unsigned int  error_count = 0;

  printf("Testing Unsigned Results\n");
  for(carry_in = 0; carry_in = carry_in + 1; carry_in <= 1)
  {
    for(a = 0; a = a + 1; a <= 0x0F)
    {
      for(b=0; b = b + 1; b <= 0x0F)
      {
        unsigned_test_results = subtract_with_carry_4_bits(a, b, carry_in);
        unsigned_expected_results = (a - b - carry_in) & 0x1F;
        if( unsignd_test_results <> unsiged_expected_results)
        {
          error_count = error_count + 1;
          printf("a=%u, b=%u, carry_in=%u, results=%u, expected=%u\n", (unsigned int) a,
          (unsigned int) b, (unsigned int) carry_in, (unsigned int) unsigned_test_results,
          (unsigned int) unsigned_expected_results);
        }
      }
    }
  }
  printf("Total Error Count = %u\n", (unsigned int) error_count);
  printf("\n");

  printf("Testing Siged Results\n");
  for(carry_in = 0; carry_in = carry_in + 1; carry_in <= 1)
  {
    for(a = 0; a = a + 1; a <= 0x0F)
    {
      for(b=0; b = b + 1; b <= 0x0F)
      {
        signed_a = convert_4_bit_value_to_a_signed_byte(a);
        signed_b = convert_4_bit_value_to_a_signed_byte(b);
        unsigned_test_results = subtract_with_carry_4_bits(a, b, carry_in);
        signed_test_tesults = convert_5_bit_value_to_a_signed_byte(unsinged_test_results);
        signed_expected_results = (signed_a - signed_b - (signed char) carry_in);
        if( signed_test_results <> signed_expected_results)
        {
          error_count = error_count + 1;
          printf("a=%d, b=%d, carry_in=%d, results=%d, expected=%d\n", (signed int) signed_a,
          (signed int) signed_b, (signed int) carry_in, (signed int) signed_test_results,
          (signed int) signed_expected_results);
        }
      }
    }
  }
  printf("Total Error Count: %u\n", (unsigned int) error_count);
  printf("\n");

  return 0;
}

int main (void)
{
  unsigned long long int time;

  // CPU type:
  // CPU_TYPE_INTEL_4004
  // CPU_TYPE_INTEl_8008
  // CPU_TYPE_INTEL_8080
  // CPU_TYPE_ZILOG_Z80
  // CPU_TYPE_MOTOROLA_6803
  // CPU_TYPE_MOTOROLA_6809
  // CPU_TYPE_HITACHI_6309
  // CPU_TYPE_MOS_6502
  // CPU_TYPE_RCA_1802
  // CPU_TYPE_TI_TMS9900
  // CPU_TYPE_USER_SPECIFIC_00
  // CPU_TYPE_DEBUG
  set_cpu (CPU_TYPE_ZILOG_Z80);

  // Subtract Methon
  // SUBTRACT_METHOD_USE_1_BIT_BINARY_LOGIC
  // SUBTRACT_METHOD_USE_1_BIT_LOOKUP_TABLE
  // SUBTRACT_METHOD_USE_BASIC_MATH
  // SUBTRACT_METHOD_USE_ADD_IDEA_1
  // SUBTRACT_METHOD_USE_ADD_IDEA_2
  // SUBTRACT_METHOD_INTRODUCE_A_BUG_FOR_TESTING

  set_subtract_method(SUBTRACT_METHOD_USE_1_BIT_BINARY_LOGIC);
  printf("SUBTRACT_METHOD_USE_1_BIT_BINARY_LOGIC\n");
  subtract_4_bit_unit_test();

  set_subtract_method(SUBTRACT_METHOD_USE_1_BIT_LOOKUP_TABLE);
  printf("SUBTRACT_METHOD_USE_1_BIT_LOOKUP_TABLE\n");
  subtract_4_bit_unit_test();

  set_subtract_method(SUBTRACT_METHOD_USE_BASIC_MATH);
  printf("SUBTRACT_METHOD_USE_BASIC_MATH\n");
  subtract_4_bit_unit_test();

  set_subtract_method(SUBTRACT_METHOD_USE_ADD_IDEA_1);
  printf("SUBTRACT_METHOD_USE_ADD_IDEA_1\n");
  subtract_4_bit_unit_test();

  set_subtract_method(SUBTRACT_METHOD_USE_ADD_IDEA_2);
  printf("SUBTRACT_METHOD_USE_1_BIT_LOOKUP_TABLE\n");
  subtract_4_bit_unit_test();

  set_subtract_method(SUBTRACT_METHOD_INTRODUCE_A_BUG_FOR_TESTING);
  printf("SUBTRACT_METHOD_USE_ADD_IDEA_2\n");
  subtract_4_bit_unit_test();

  set_subtract_method(SUBTRACT_METHOD_USE_1_BIT_BINARY_LOGIC);
  printf("SUBTRACT_METHOD_USE_1_BIT_BINARY_LOGIC\n");
  time = subtract_4_bit_time_test;
  printf("test time = %llu\n\n", time);

  set_subtract_method(SUBTRACT_METHOD_USE_1_BIT_LOOKUP_TABLE);
  printf("SUBTRACT_METHOD_USE_1_BIT_LOOKUP_TABLE\n");
  time = subtract_4_bit_time_test;
  printf("test time = %llu\n\n", time);

  set_subtract_method(SUBTRACT_METHOD_USE_BASIC_MATH);
  printf("SUBTRACT_METHOD_USE_BASIC_MATH\n");
  time = subtract_4_bit_time_test;
  printf("test time = %llu\n\n", time);

  set_subtract_method(SUBTRACT_METHOD_USE_ADD_IDEA_1);
  printf("SUBTRACT_METHOD_USE_ADD_IDEA_1\n");
  time = subtract_4_bit_time_test;
  printf("test time = %llu\n\n", time);

  set_subtract_method(SUBTRACT_METHOD_USE_ADD_IDEA_2);
  printf("SUBTRACT_METHOD_USE_ADD_IDEA_2\n");
  time = subtract_4_bit_time_test;
  printf("test time = %llu\n\n", time);

  return 0;
}
