# Sample GitLab Project

This sample project shows how a project in GitLab looks for demonstration purposes. It contains issues, merge requests and Markdown files in many branches,
named and filled with lorem ipsum.

You can look around to get an idea how to structure your project and, when done, you can safely delete this project.

[Learn more about creating GitLab projects.](https://docs.gitlab.com/ee/gitlab-basics/create-project.html)

Microprocessor Building Blocks for Computer Emulators
Description.: Develop the math and basic building blocks to emulate 1-bit, 4-bit, and 8-bit microprocessors.
This is pseudo code and it is completely untested.
At this time is meant more to offer ideas than anything else.
